%macro exit 0
	mov rax, 60
	mov rdi, 0
	syscall
%endmacro

section .data
	text db "Hello World, this is a test",10,0

section .text
	global _start

_start:
	mov rax, text
	mov rbx, 0
	mov rcx, 20
	call _printSubString
	exit

_printSubString:
	add rax, rbx
	push rax
	sub rcx, rbx

	mov rax, 1
	mov rdi, 1
	pop rsi
	mov rdx, rcx
	syscall
	ret
