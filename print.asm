section .data
	line db "Hello World",10,0

section .text
	global _start

_start:
	mov rax, line       ;moves text in rax to be copied
	call _print         ;calls _print function
	call _exit          ;calls _exit function

_print:
	push rax            ;saves the value of rax is stack
	mov rbx, 0          ;initializes rbx to 0

_printLoop:             ;beginnig of _printLoop
	inc rax             ;increments rax and rbx with every loop
	inc rbx
	mov cl, [rax]       ;moves one byte of rax as a pointer in cl
	cmp cl, 0           ;compares the value in cl to 0 which indicates the end of the string
	jne _printLoop      ;loops until we reach the end of the string

	mov rax, 1          ;moves 1 into rax for system out
	mov rdi, 1          ;moves 1 into rdi for destination 1
	pop rsi             ;moves the top of the stack (the value taken from rax at the beginning just after _print) to rsi (place in memory to be printed)
	mov rdx, rbx        ;moves the value in rbx to rdx for number of bytes to be printed
	syscall             ;having set all the regiters to print, this part calls the kernel
	ret                 ;lastly we return to the caller

_exit:                  ;we jump here to exit the program
	mov rax, 60         ;moves 60 to rax for system exit
	mov rdi, 0          ;moves 0 to rdi for no error
	syscall             ;calls the kernel
