%macro exit 0
	mov rax, 60
	mov rsi, 0
	syscall
%endmacro

section .data
	text1 db "Hello World",10,0
	text2 db "Hello World",10,0
	true db "Y"
	false db "N"

section .text
	global _start

_start:
	mov rax, text1
	mov rbx, text2
	call _strcmp
	exit

_strcmp:
	mov cl, [rax]
	mov dl, [rbx]
	cmp cl, dl
	jne _notEqual
	inc rax
	inc rbx
	cmp cl, 0
	je _end1
	cmp dl, 0
	je _end2
	jmp _strcmp

_end1:
	cmp dl, 0
	je _equal
	jne _notEqual

_end2:
	cmp cl, 0
	je _equal
	jne _notEqual

_equal:
	mov rsi, true
	jmp _print

_notEqual:
	mov rsi, false
	jmp _print

_print:
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
	ret
