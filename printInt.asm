%macro exit 0
	mov rax, 60
	mov rdi, 0
	syscall
%endmacro

section .data
	num dd 4294967295

section .bss
	integer resb 10

section .text
	global _start

_start:
	mov rax, [num]
	call _printInt
	exit

_printInt:
	mov rbx, 10
	mov rcx, 0

_loop1:
	mov rdx, 0
	div rbx
	add rdx, 48
	push rdx
	inc rcx
	cmp al, 0
	jne _loop1

	mov rax, integer
	mov rdx, 0

_loop2:
	pop rbx
	mov [rax], bl
	inc rax
	inc rdx
	cmp rdx, rcx
	jne _loop2
	
	mov rax, 1
	mov rdi, 1
	mov rsi, integer
	syscall

	ret
