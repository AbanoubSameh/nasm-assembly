%macro exit 0
	mov rax, 60
	mov rdi, 0
	syscall
%endmacro

section .data
	text db "hello World",10,0

section .text
	global _start

_start:
	mov rax, text
	call _toUpperCase
	exit

_toUpperCase:
	mov rcx, 0

_loop:
	mov cl, [rax]
	cmp cl, 97
	jge _greater
_here:
	inc rax
	cmp rcx, 0
	jne _loop

	mov rax, 1
	mov rdi, 1
	mov rsi, text
	mov rdx, 13
	syscall
	ret

_greater:
	cmp cl, 122
	jle _changeCase
	jmp _here

_changeCase:
	sub cl, 32
	mov [rax], cl
	jmp _here
