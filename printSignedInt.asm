%macro exit 0
	mov rax, 60
	mov rdi, 0
	syscall
%endmacro

section .data
	num dd 2147483648

section .bss
	integer resb 12

section .text
	global _start

_start:
	mov eax, [num]
	call _printInt
	exit

_printInt:
	mov rbx, 10
	mov rcx, 0
	cmp eax, 2147483647
	mov rdi, 0
	jbe _loop1
	mov rdi, 1
	neg eax
	inc rcx

_loop1:
	mov rdx, 0
	div rbx
	add rdx, 48
	push rdx
	inc rcx
	cmp al, 0
	jne _loop1

	mov rax, integer
	mov rdx, 0
	cmp rdi, 1
	jne _loop2
	push 45

_loop2:
	pop rbx
	mov [rax], bl
	inc rax
	inc rdx
	cmp rdx, rcx
	jne _loop2
	
	mov rax, 1
	mov rdi, 1
	mov rsi, integer
	syscall

	ret
